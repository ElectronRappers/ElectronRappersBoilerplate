# ElectronRappersBoilerplate

Boilerplate for ElectronRappers

Currently wraps http://www.example.com/ as the dummy website.

It also uses dummy icons from https://www.christianengvall.se/electron-app-icons/ 's tutorial.

It should not be forked but instead copied as it is just to get a project started without looking for all the things to change

## Things to change

CTRL-F the entire project to find and replace the following text:

*  http://www.example.com/ -> The Wrapped Site
*  Boilerplate Desktop -> [Wrapped Site Name] Desktop
*  https://gitlab.com/ElectronRappers/ElectronRappersBoilerplate.git -> current project git file
*  alpha.boilerplate.electronrapper.net -> alpha.[site name].electronrapper.net
*  /opt/Boilerplate Desktop/build/icons/icon.png -> /opt/[Wrapped Site Name]/build/icons/icon.png

Also don't forget to change the icons for the app.